# Project Instructions 🔖

React SPK Demo 🔥

# Getting started 🔨


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000] to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Development Environment and Architecture 🧰

- TypeScript
- React

## API and JS Integration

- We are using an array of beers from `https://jsonplaceholder.typicode.com/` located in `data.json`

## Sessions

### 1. ReactJS🤖

- What is React?
- Why React?
- React Skeleton
- React Components
- Demo
    - 1-react-demo-start
- React Demo using data from `https://jsonplaceholder.typicode.com/`

### 2.Redux and Friends 🦾

- Why Redux?
- Add Redux
- React Skeleton
- React Component
- React Hooks (useState, useEffect)

## Learn More 📚

- React https://reactjs.org/
- Redux https://redux.js.org/
- Puma Designsystem http://puma-designsystem-web.lyn.spk.no/
