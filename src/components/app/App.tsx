import React from 'react';
import Welcome from '../welcome/Welcome';

import './App.css';

const App = () => {
  return (
    <div className="App">
      <Welcome />
    </div>
  );
};

export default App;
